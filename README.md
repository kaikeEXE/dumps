# merlin-user 11 RP1A.200720.011 V12.5.6.0.RJOEUXM release-keys
merlin-user 11 RP1A.200720.011  release-keys
merlinnfc-user 11 RP1A.200720.011  release-keys
- manufacturer: xiaomi
- platform: mt6768
- codename: merlin
- flavor: merlin-user
merlin-user
merlinnfc-user
- release: 11
- id: RP1A.200720.011
- incremental: V12.5.6.0.RJOEUXM
- tags: release-keys
- fingerprint: Redmi/merlin_eea/merlin:11/RP1A.200720.011/V12.5.6.0.RJOEUXM:user/release-keys
Redmi/merlin_eea/merlin:11/RP1A.200720.011/V12.5.6.0.RJOEUXM:user/release-keys
Redmi/merlinnfc_eea/merlinnfc:11/RP1A.200720.011/V12.5.6.0.RJOEUXM:user/release-keys
- is_ab: false
- brand: Redmi
- branch: merlin-user-11-RP1A.200720.011-V12.5.6.0.RJOEUXM-release-keys
merlin-user-11-RP1A.200720.011--release-keys
merlinnfc-user-11-RP1A.200720.011--release-keys
- repo: redmi_merlin_dump
